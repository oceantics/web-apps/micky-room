from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from puput import urls as puput_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

urlpatterns = [
    path(settings.ADMIN_URL, admin.site.urls),
    path("cms/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path("comments/", include("django_comments_xtd.urls")),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        path("__debug__/", include("debug_toolbar.urls")),
    ]

elif not settings.DEBUG:
    urlpatterns += [path("health-check/", include("health_check.urls"))]

urlpatterns = urlpatterns + [
    path("", include("websites.cumpa.urls")),
    path("", include(puput_urls)),
    path("", include(wagtail_urls)),
]
