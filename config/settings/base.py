from pathlib import Path

import environ

env = environ.Env()
ROOT_DIR = Path(__file__).resolve(strict=True).parent.parent.parent
APPS_DIR = ROOT_DIR / "websites"

READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=True)
if READ_DOT_ENV_FILE:
    env.read_env(str(ROOT_DIR / ".env"))

SECRET_KEY = env("DJANGO_SECRET_KEY")

DEBUG = env.bool("DJANGO_DEBUG")

ALLOWED_HOSTS = env.list("DJANGO_ALLOWED_HOSTS")

DJANGO_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
)

WAGTAIL_APPS = (
    "wagtail.contrib.forms",
    "wagtail.contrib.redirects",
    "wagtail.contrib.modeladmin",
    "wagtail.embeds",
    "wagtail.sites",
    "wagtail.users",
    "wagtail.snippets",
    "wagtail.documents",
    "wagtail.images",
    "wagtail.search",
    "wagtail.admin",
    "wagtail.core",
    "wagtail.api.v2",
    "wagtail.contrib.routable_page",
    "wagtail.contrib.sitemaps",
)

THIRD_PARTY_APPS = (
    "modelcluster",
    "taggit",
    "rest_framework",
    "wagtailmenus",
    "django_social_share",
    "puput",
    "colorful",
    "django_comments_xtd",
    "django_comments",
    "snowpenguin.django.recaptcha3",
)

WEBSITES = (
    "rupicola.apps.RupicolaConfig",
    "websites.cumpa.apps.CumpaConfig",
    "websites.oceantics.apps.OceanticsConfig",
)

INSTALLED_APPS = WEBSITES + WAGTAIL_APPS + THIRD_PARTY_APPS + DJANGO_APPS

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "wagtail.contrib.redirects.middleware.RedirectMiddleware",
]

ROOT_URLCONF = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "wagtail.contrib.settings.context_processors.settings",
                "wagtailmenus.context_processors.wagtailmenus",
            ],
        },
    },
]

WSGI_APPLICATION = "config.wsgi.application"

DATABASES = {
    "default": env.db("DJANGO_DATABASE_URL"),
}
DATABASES["default"]["ATOMIC_REQUESTS"] = True

LANGUAGE_CODE = "es-PE"
TIME_ZONE = "America/Lima"
USE_I18N = True
USE_L10N = True
USE_TZ = True

FIXTURE_DIRS = (str(APPS_DIR / "fixtures"),)

STATICFILES_DIRS = [
    str(APPS_DIR / "static"),  # noqa
]
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]
STATIC_ROOT = str(ROOT_DIR / "staticfiles")
STATIC_URL = "/static/"

MEDIA_ROOT = str(ROOT_DIR / "media")
MEDIA_URL = "/media/"

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

ADMIN_URL = env("DJANGO_ADMIN_URL", default="admin/")

WAGTAIL_SITE_NAME = "Oceantics CMS"

PUPUT_AS_PLUGIN = True

COMMENTS_APP = "django_comments_xtd"
COMMENTS_XTD_MAX_THREAD_LEVEL = 10
COMMENTS_XTD_CONFIRM_EMAIL = False
COMMENTS_XTD_LIST_ORDER = ("-thread_id", "order")

RECAPTCHA_PRIVATE_KEY = env("DJANGO_RECAPTCHA_PRIVATE_KEY", None)
RECAPTCHA_PUBLIC_KEY = env("DJANGO_RECAPTCHA_PUBLIC_KEY", None)
RECAPTCHA_SCORE_THRESHOLD = 0.75
