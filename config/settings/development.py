from .base import *  # noqa
from .base import env

TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG  # noqa

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

INSTALLED_APPS += (  # noqa
    "debug_toolbar",
    "django_extensions",
)

MIDDLEWARE += [  # noqa
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel"],
    "SHOW_TEMPLATE_CONTEXT": True,
}

INTERNAL_IPS = ("127.0.0.1", "127.0.1.1")

BASE_URL = env("BASE_URL", default="http://example.com")

STATICFILES_DIRS += [  # noqa
    str(APPS_DIR / "oceantics/static"),  # noqa
    str(APPS_DIR / "cumpa/static"),  # noqa
]
