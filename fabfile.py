import json

from fabric import Connection
from fabric.tasks import task


@task
def deploy(c):
    with open("remote_config.json", "r") as f:
        configuration = json.load(f)
        remote = Connection(
            configuration.get("host"),
            user=configuration.get("user"),
            port=22,
            connect_kwargs=configuration.get("connect_kwargs"),
        )
        user = configuration.get("app_user")
        remote.sudo(f"bash {configuration.get('update_script')}", user=user)
        remote.run("supervisorctl reload")
