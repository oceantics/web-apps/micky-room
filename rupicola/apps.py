from django.apps import AppConfig


class RupicolaConfig(AppConfig):
    name = "rupicola"
