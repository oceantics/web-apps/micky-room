from django.apps import AppConfig


class OceanticsConfig(AppConfig):
    name = "websites.oceantics"
