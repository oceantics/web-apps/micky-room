from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.blocks import CharBlock, StructBlock, TextBlock
from wagtail.core.fields import StreamField
from wagtail.core.models import Page


class LandingPage(Page):
    name = models.TextField()
    abstract = models.TextField(blank=True)
    image = models.TextField(blank=True)
    logo = models.TextField(blank=True)
    links = StreamField(
        [
            ("link", TextBlock(form_classname="full title")),
        ],
        null=True,
        blank=True,
    )
    services_title = models.TextField(blank=True)
    services_link = models.TextField(blank=True)
    services = StreamField(
        [
            (
                "service",
                StructBlock(
                    [
                        ("icon", TextBlock()),
                        ("name", CharBlock(max_length=255)),
                        ("abstract", TextBlock()),
                    ]
                ),
            ),
        ],
        null=True,
        blank=True,
    )
    company_title = models.TextField(blank=True)
    company_subtitle = models.TextField(blank=True)
    company_abstract = models.TextField(blank=True)
    company_image = models.TextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("name"),
        FieldPanel("abstract"),
        FieldPanel("image"),
        FieldPanel("logo"),
        StreamFieldPanel("links"),
        FieldPanel("services_title"),
        FieldPanel("services_link"),
        StreamFieldPanel("services"),
        FieldPanel("company_title"),
        FieldPanel("company_subtitle"),
        FieldPanel("company_abstract"),
        FieldPanel("company_image"),
    ]
