from .about import AboutSection  # noqa
from .contact import ContactSection  # noqa
from .hero import HeroSection  # noqa
from .resume import ResumeSection  # noqa
