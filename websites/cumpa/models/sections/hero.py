from django.utils.translation import gettext as _

from wagtail.core.blocks import CharBlock, ListBlock, StreamBlock, StructBlock, URLBlock
from wagtail.images.blocks import ImageChooserBlock


class SocialLink(StructBlock):
    name = CharBlock(label=_("Name"), classname="title", required=False)
    url = URLBlock(required=True)
    icon = ImageChooserBlock(required=True)


class HeroSection(StreamBlock):
    i_am = CharBlock(label=_("I Am"), max_length=150)
    presentation = CharBlock(label=_("Presentation"), max_length=500)
    job_positions = ListBlock(CharBlock(required=True, max_length=250))
    social_links = ListBlock(SocialLink())
    photo = ImageChooserBlock(required=True)

    class Meta:
        block_counts = {
            "i_am": {"max_num": 1},
            "presentation": {"max_num": 1},
            "job_positions": {"max_num": 1},
            "social_links": {"max_num": 1},
            "photo": {"max_num": 1},
        }
        max_num = 5
        template = "cumpa/sections/hero.html"
