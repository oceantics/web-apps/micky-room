from django.db.models import TextChoices
from django.utils.translation import gettext as _

from wagtail.core.blocks import (
    CharBlock,
    ChoiceBlock,
    DecimalBlock,
    ListBlock,
    RawHTMLBlock,
    StreamBlock,
    StructBlock,
    TextBlock,
)


class DetailTimeline(StructBlock):
    title = CharBlock(max_length=250)
    dates = CharBlock(max_length=350)
    content = TextBlock()


class DetailSkill(StructBlock):
    name = CharBlock(max_length=350)
    quantity = DecimalBlock(max_value=100, min_value=1, max_digits=4, decimal_places=1)
    label = CharBlock(max_length=350, required=False)


class Timeline(StructBlock):
    title = CharBlock(max_length=250)
    icon = RawHTMLBlock()
    details = ListBlock(DetailTimeline())


class Skill(StructBlock):
    class TypePresentation(TextChoices):
        BAR = "BAR", _("Bar")
        STAR = "STAR", _("Star")

    title = CharBlock(max_length=250)
    presentation = ChoiceBlock(choices=TypePresentation.choices)
    details = ListBlock(DetailSkill())


class ResumeSection(StreamBlock):
    description = TextBlock(required=False)
    timelines = ListBlock(Timeline())
    skills = ListBlock(Skill())

    class Meta:
        block_counts = {
            "description": {"max_num": 1},
            "timelines": {"max_num": 1},
            "skills": {"max_num": 1},
        }
        max_num = 3
        template = "cumpa/sections/resume.html"
