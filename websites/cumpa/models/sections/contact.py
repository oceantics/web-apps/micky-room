from wagtail.core.blocks import StreamBlock, TextBlock


class ContactSection(StreamBlock):
    description = TextBlock(required=False)
    contact_number = TextBlock(required=False)
    address_location = TextBlock(required=False)
    contact_email = TextBlock(required=False)

    class Meta:
        block_counts = {
            "description": {"max_num": 1},
            "contact_number": {"max_num": 1},
            "address_location": {"max_num": 1},
            "contact_email": {"max_num": 1},
        }
        max_num = 4
        template = "cumpa/sections/contact.html"
