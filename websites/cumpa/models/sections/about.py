from django.utils.translation import gettext as _

from wagtail.core.blocks import (
    CharBlock,
    IntegerBlock,
    ListBlock,
    StreamBlock,
    StructBlock,
    TextBlock,
)
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.images.blocks import ImageChooserBlock


class PersonalInfo(StructBlock):
    personal_label = CharBlock(max_length=250)
    personal_value = CharBlock(max_length=350)


class Service(StructBlock):
    title = CharBlock(max_length=350)
    image = DocumentChooserBlock()
    description = CharBlock(max_length=500)


class CountUp(StructBlock):
    icon = DocumentChooserBlock()
    count = IntegerBlock(max_length=350)
    label = CharBlock(max_length=350)


class Testimonial(StructBlock):
    photo = ImageChooserBlock()
    content = TextBlock()
    full_name = CharBlock(max_length=350)
    job = CharBlock(max_length=350, required=False)


class AboutSection(StreamBlock):
    about_me = TextBlock(label=_("About"))
    picture = ImageChooserBlock(label=_("Picture"))
    professional_profile = CharBlock(label=_("Professional Profile"), max_length=350)
    personal_info = ListBlock(PersonalInfo())
    cv_document = DocumentChooserBlock()
    hire_me = CharBlock(label=_("Hire Me"), max_length=255)
    count_up = ListBlock(CountUp())
    services = ListBlock(Service)
    testimonials = ListBlock(Testimonial)

    class Meta:
        max_num = 9
        template = "cumpa/sections/about.html"
        block_counts = {
            "about_me": {"max_num": 1},
            "picture": {"max_num": 1},
            "professional_profile": {"max_num": 1},
            "personal_info": {"max_num": 1},
            "cv_document": {"max_num": 1},
            "hire_me": {"max_num": 1},
            "count_up": {"max_num": 1},
            "services": {"max_num": 1},
            "testimonials": {"max_num": 1},
        }
