from django.db import models
from django.utils.safestring import mark_safe

from model_utils.models import TimeStampedModel
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, StreamFieldPanel
from wagtail.api import APIField
from wagtail.core.blocks import CharBlock, ListBlock, StructBlock
from wagtail.core.fields import StreamField
from wagtail.core.models import Orderable, Page
from wagtail.images.api.fields import ImageRenditionField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.snippets.models import register_snippet


class Category(TaggedItemBase):
    content_object = ParentalKey(
        "cumpa.Portfolio",
        on_delete=models.SET_NULL,
        related_name="categorized_items",
        null=True,
        blank=True,
    )


class Skill(TaggedItemBase):
    content_object = ParentalKey(
        "cumpa.Portfolio",
        on_delete=models.SET_NULL,
        related_name="skill_items",
        null=True,
        blank=True,
    )


class CumpaPortfolioOrderable(Orderable):
    page = ParentalKey("cumpa.CumpaPortfolioPage", related_name="cumpa_portfolios")
    portfolio = models.ForeignKey("cumpa.Portfolio", on_delete=models.CASCADE)

    panels = [SnippetChooserPanel("portfolio")]


@register_snippet
class Author(TimeStampedModel):
    names = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    birth_day = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return f"{self.names} {self.last_name}"


@register_snippet
class Portfolio(TimeStampedModel, ClusterableModel):
    title = models.CharField(max_length=350)
    description = models.TextField(blank=True)
    completed_on = models.DateField(null=True, blank=True)
    author = models.ForeignKey(Author, null=True, blank=True, on_delete=models.SET_NULL)
    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    detail = models.TextField(blank=True)
    skills = TaggableManager(through=Skill, blank=True, related_name="skills")
    tags = TaggableManager(through=Category, blank=True, related_name="tags")

    panels = [
        FieldPanel("title"),
        FieldPanel("description"),
        FieldPanel("completed_on"),
        SnippetChooserPanel("author"),
        ImageChooserPanel("image"),
        FieldPanel("detail"),
        FieldPanel("skills"),
        FieldPanel("tags"),
    ]

    api_fields = [
        APIField("title"),
        APIField("description"),
        APIField("completed_on"),
        APIField("author"),
        APIField("image"),
        APIField("detail"),
        APIField("tags"),
        APIField("skills"),
        APIField("author_html"),
        APIField("completed_on_html"),
        APIField("title_html"),
        APIField(
            "image_cover",
            serializer=ImageRenditionField("fill-600x450", source="image"),
        ),
    ]

    class Meta:
        verbose_name = "Portfolio"
        verbose_name_plural = "Portfolios"

    def __str__(self):
        return self.title

    @property
    def completed_on_html(self):
        return self.completed_on.strftime("%B %d, %Y")

    @property
    def author_html(self):
        return self.author.full_name

    @property
    def title_html(self):
        words = self.title.split(" ")
        if len(words) == 1:
            return mark_safe(f'<span class="base-color"> {words[0]}</span>')
        elif len(words) > 1:
            return mark_safe(
                f'{" ".join(words[0:-1])} <span class="base-color"> {"".join(words[-1:])}</span>'
            )
        return ""


class Filter(StructBlock):
    link = CharBlock(max_length=250)
    data_filter = CharBlock(max_length=250)
    title = CharBlock(max_length=350)


class CumpaPortfolioPage(TimeStampedModel, Page):
    description = models.CharField(max_length=500, blank=True)
    page_filters = StreamField([("filters", ListBlock(Filter()))])
    show_in_site = models.BooleanField(default=False)

    content_panels = Page.content_panels + [
        FieldPanel("description", classname="full"),
        FieldPanel("show_in_site", classname="full"),
        StreamFieldPanel("page_filters", classname="full"),
        InlinePanel("cumpa_portfolios", label="Related Works"),
    ]

    parent_page_types = ["CumpaHomePage"]

    api_fields = [APIField("cumpa_portfolios")]

    class Meta:
        ordering = ("-modified",)

    @property
    def portfolios(self):
        return Portfolio.objects.filter(
            pk__in=self.cumpa_portfolios.all().values_list("portfolio", flat=True)
        )
