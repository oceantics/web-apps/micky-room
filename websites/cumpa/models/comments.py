from django_comments_xtd.models import XtdComment
from puput.models import EntryPage


class CumpaComment(XtdComment):
    @property
    def css_level(self):
        return 3 * self.level

    @classmethod
    def get_recent_comments(cls, site_id):
        return CumpaComment.objects.filter(
            site_id=site_id, is_public=True, is_removed=False
        ).order_by("-submit_date")[:3]

    def get_num_comments(self):
        return CumpaComment.objects.filter(
            object_pk=self.object_pk, is_public=True, is_removed=False
        ).count()

    def save(self, *args, **kwargs):
        force_insert = kwargs.get("force_insert", False)
        super().save(*args, **kwargs)
        if force_insert:
            entry_page = EntryPage.objects.get(pk=self.object_pk)
            entry_page.num_comments = CumpaComment.objects.filter(
                object_pk=entry_page.pk, is_public=True, is_removed=False
            ).count()
            entry_page.save(update_fields=("num_comments",))
        return self
