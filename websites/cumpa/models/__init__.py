from django.conf import settings

from .comments import CumpaComment  # noqa
from .home import CumpaHomePage  # noqa
from .portfolio import CumpaPortfolioPage  # noqa

settings.COMMENTS_XTD_FORM_CLASS = "websites.cumpa.forms.CumpaCommentForm"
settings.COMMENTS_XTD_MODEL = "websites.cumpa.models.CumpaComment"
settings.PUPUT_COMMENTS_PROVIDER = settings.COMMENTS_XTD_MODEL
