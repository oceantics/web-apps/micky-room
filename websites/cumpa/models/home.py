import logging

from django.utils.translation import gettext as _

from puput.models import BlogPage
from wagtail.admin.edit_handlers import StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Page

from .portfolio import CumpaPortfolioPage
from .sections import AboutSection, ContactSection, HeroSection, ResumeSection

logger = logging.getLogger(__name__)


class CumpaHomePage(Page):
    hero = StreamField(HeroSection())
    about = StreamField(AboutSection())
    resume = StreamField(ResumeSection())
    contact = StreamField(ContactSection())

    content_panels = Page.content_panels + [
        StreamFieldPanel("hero", heading="Hero"),
        StreamFieldPanel("about", heading="About"),
        StreamFieldPanel("resume", heading="Resume"),
        StreamFieldPanel("contact", heading="Contact"),
    ]

    def get_context(self, request):
        context = super().get_context(request)
        try:
            blog_page = BlogPage.objects.in_site(self.get_site()).last()
            context["cumpaportfolio_page"] = CumpaPortfolioPage.objects.filter(
                show_in_site=True
            ).last()
            context["cumpablog_page"] = blog_page
            context["entries"] = blog_page.get_entries() if blog_page else []
        except Exception as e:
            logger.error(str(e))
        return context

    class Meta:
        verbose_name = verbose_name_plural = _("Home Page - Cumpa's Website")
