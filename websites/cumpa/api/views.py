from wagtail.api.v2.views import BaseAPIViewSet

from ..models.portfolio import Portfolio


class PortfolioSnippetAPIViewSet(BaseAPIViewSet):

    model = Portfolio
