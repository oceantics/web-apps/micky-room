from django_comments_xtd.forms import XtdCommentForm
from snowpenguin.django.recaptcha3.fields import ReCaptchaField


class CumpaCommentForm(XtdCommentForm):

    captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["followup"].widget.attrs["class"] = "form-check-input"
