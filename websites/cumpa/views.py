from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import CreateView, DetailView, View

from puput.models import EntryPage

from .forms import CumpaCommentForm
from .models import CumpaComment


class CumpaCommentCreateView(CreateView):
    model = CumpaComment
    form_class = CumpaCommentForm
    template_name = "comments/form.html"

    def get_form(self, form_class=None):
        args = (self.get_entry(),)
        kwargs = self.get_form_kwargs()
        if "instance" in kwargs.keys():
            del kwargs["instance"]
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(*args, **kwargs)

    def get_entry(self):
        return EntryPage.objects.get(pk=self.kwargs.get("entry_id"))

    def form_valid(self, form):
        site = get_current_site(self.request)
        comment_data = form.get_comment_create_data(site_id=site.pk)
        comment_data["ip_address"] = self.request.META.get("REMOTE_ADDR")
        CumpaComment.objects.create(**comment_data)
        return redirect("cumpa-list-comments", *(self.kwargs.get("entry_id"),))


class CumpaCommentsView(DetailView):
    model = EntryPage
    template_name = "cumpa/sections/comments.html"


class CumpaCountCommentsView(View):
    def get(self, request, *args, **kwargs):
        instance = get_object_or_404(EntryPage, pk=kwargs.get("pk"))
        return HttpResponse(
            f"<i class='fa fa-comments'></i>{ instance.num_comments}",
            content_type="text/html",
        )
