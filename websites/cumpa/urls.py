from django.conf import settings
from django.urls import path, re_path

from wagtail.api.v2.router import WagtailAPIRouter
from wagtail.api.v2.views import PagesAPIViewSet
from wagtail.contrib.sitemaps.views import sitemap

from .api.views import PortfolioSnippetAPIViewSet
from .views import CumpaCommentCreateView, CumpaCommentsView, CumpaCountCommentsView

api_router = WagtailAPIRouter("wagtailapi")
api_router.register_endpoint("pages", PagesAPIViewSet)
api_router.register_endpoint("portfolios", PortfolioSnippetAPIViewSet)

urlpatterns = [
    path("api/v2/", api_router.urls),
    re_path(r"^sitemap\.xml$", sitemap),
    path(
        "entry/<int:entry_id>/comments/create/",
        CumpaCommentCreateView.as_view(),
        name="cumpa-create-comment",
    ),
    path(
        "entry/<int:pk>/comments/",
        CumpaCommentsView.as_view(),
        name="cumpa-list-comments",
    ),
    path(
        "entry/<int:pk>/count-comments/",
        CumpaCountCommentsView.as_view(),
        name="cumpa-count-comments",
    ),
]

if settings.DEBUG:
    from django.views.generic import TemplateView

    urlpatterns += [
        re_path(r"^test404/$", TemplateView.as_view(template_name="404.html")),
        re_path(r"^test500/$", TemplateView.as_view(template_name="500.html")),
    ]
