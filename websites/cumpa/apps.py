from django.apps import AppConfig


class CumpaConfig(AppConfig):
    name = "websites.cumpa"
