class PortfolioModel {
    portfolioID = null;
    loaderModel = null;
    modal = null;

    constructor(modal) {
        this.loaderModel = $("#modal-loader");
        this.modal = modal;
    }

    async modalDetail() {
        const responseJSON = await fetch(this.getPortfolioURL());
        return await responseJSON.json();
    }

    setPortfolioID(portfolioID) {
        this.portfolioID = portfolioID;
    }

    getPortfolioURL() {
        return `/api/v2/portfolios/${this.portfolioID}/`;
    }

    renderModal() {
        portfolioModel.modalDetail().then((data) => {
            const content = $('#modal-content', this.modal);
            content.html(this.modalContent(data));
        });
    }

    modalContent(data) {
        return `<div class="text-center"><h2 class="mb-3">${data.title_html}</h2>
        <p class="max-width-450">${data.description}</p>
        </div><div class="row contact-info mt-5"><div class="col-md-4"><div class="text-center"><div>
        <h6>Created by:</h6><p class="mb-0">${data.author_html}</p></div></div></div>
        <div class="col-md-4 p-title-border mt-3 mt-md-0"><div class="text-center"><div>
        <h6 class="contact_detail-title">Completed on:</h6><p class="mb-0">${data.completed_on_html}</p>
        </div></div></div><div class="col-md-4 mt-3 mt-md-0"><div class="text-center"><div><h6>Skills:</h6>
        <p class="mb-0">${data.skills.join(' / ')}</p></div></div></div></div>
        <div class="mt-5"><div class="portfolio-single-item image-border">
        <img src="${data.image_cover.url}" class="d-block" alt="${data.title}"></div></div>
        <div class="text-md-left mt-5"><p class="my-3">${data.detail.split('\n').join('</p><p class="my-3">')}</p></div>`;
    }

    renderPreLoadModal() {
        const content = $('#modal-content', this.modal);
        content.html(this.loaderModel);
    }
}


class CommentModel {

    static setEventLinks() {
        const replyLinks = document.getElementsByClassName("comment-reply-link");
        for (let i = 0; i < replyLinks.length; i++) {
            replyLinks[i].addEventListener("click", (e) => {
                document.getElementById("id_reply_to").value = e.target.dataset.key;
                cancelDiv.classList.remove("invisible");
                cancelDiv.classList.add("visible");
            });
        }
    }

    static setCountComments() {
        CommentModel.getCountComments().then(html => countA.innerHTML = html);
    }

    static initForm() {
        const commentForm = document.getElementById("id_formComment");
        const commentDiv = document.getElementById("id_DivComment");
        const commentsDiv = document.getElementById("user-comments");
        const btnCancel = document.getElementById("blog-cancel-btn");
        const btnSubmit = document.getElementById("blog-submit-btn");

        btnCancel.addEventListener("click", (e) => {
            document.getElementById("id_reply_to").value = 0;
            cancelDiv.classList.remove("visible");
            cancelDiv.classList.add("invisible");
        });
        btnSubmit.addEventListener("click", (e) => {
            const formData = new FormData(commentForm);
            CommentModel.sendComment(formData).then((html) => {
                commentsDiv.innerHTML = html;
                setTimeout(() => {
                    CommentModel.setEventLinks();
                    CommentModel.setCountComments();
                }, 1000);
            });
            CommentModel.getFormComment().then(html => commentDiv.innerHTML = html);
            setTimeout(() => {
                CommentModel.initForm();
                loadGrecaptcha();
            }, 1000);
            return false;
        });
    }

    static async sendComment(formData) {
        const responseHTML = await fetch(commentCreateURL, {
            method: 'POST',
            body: formData,
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        return await responseHTML.text();
    }

    static async getFormComment() {
        const responseHTML = await fetch(commentCreateURL);
        return await responseHTML.text();
    }

    static async getCountComments() {
        const responseHTML = await fetch(commentCountURL);
        return await responseHTML.text();
    }
}


const modal = $('#portfolio-single').on('show.bs.modal', function (e) {
    const portfolioID = e.relatedTarget.dataset.key;
    portfolioModel.setPortfolioID(portfolioID);
    portfolioModel.renderModal();
}).on('hidden.bs.modal', function (e) {
    portfolioModel.renderPreLoadModal();
});

const cancelDiv = document.getElementById("id_divCancel");
const countA = document.getElementById("count-comments");

const portfolioModel = new PortfolioModel(modal);

if (document.getElementById("id_formComment")) {
    CommentModel.initForm();
    CommentModel.setEventLinks();
}
