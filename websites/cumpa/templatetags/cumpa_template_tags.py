from decimal import ROUND_HALF_UP, Decimal, localcontext

from django import template
from django.contrib.sites.shortcuts import get_current_site
from django.utils.html import mark_safe

from puput.models import EntryPage

from ..models import CumpaComment

register = template.Library()


@register.filter
def int_value(value):
    return int(value)


@register.simple_tag
def is_type(instance, type_name):
    return instance.__class__.__name__ == type_name


@register.simple_tag
def middle_of_list(list_objects):
    with localcontext() as ctx:
        ctx.rounding = ROUND_HALF_UP
        middle = Decimal(len(list_objects)) / 2
        return middle.to_integral_value()


@register.simple_tag
def parse_star(value, color):
    if type(value) == str:
        value = float(value)
    value = value if 0 < int_value(value) <= 5 else 0
    content = [""]
    for index in range(1, 6):
        content.append('<li class="list-inline-item">')
        star = '<i class="fa fa-star-o data-text-color" '
        if index - 0.5 == value:
            star = '<i class="fa fa-star-half-empty data-text-color" '
        elif value == 0 or index > value:
            content.append(star)
        elif index <= value:
            star = '<i class="fa fa-star data-text-color" '
        content.append(star)
        content.append(f'data-color="{color}"></i>')
        content.append("</li>")
    return mark_safe("".join(content))


@register.simple_tag
def recent_posts(blog, current_entry=None):
    posts = (
        EntryPage.objects.descendant_of(blog)
        .live()
        .order_by("-date")
        .select_related("owner")
    )
    if current_entry:
        posts = posts.exclude(pk=current_entry.pk)
    return posts[:5]


@register.simple_tag(takes_context=True)
def recent_comments(context):
    site = get_current_site(context.get("request"))
    comments = CumpaComment.get_recent_comments(site.pk)
    return comments


@register.simple_tag
def archive_posts(blog):
    return blog.get_entries().datetimes("date", "month", order="DESC")
